

class BoxGroup {

    constructor(opts) {
        if(opts.parent === undefined || opts.name === undefined) {
            console.error('Please define a name / element for this group!');
            return;
        }
        this.rootElement = document.createElement('section');
        this.rootElement.className = 'group';
        this.parent = opts.parent;

        let titleElement = document.createElement('p');
        titleElement.innerText = opts.name;
        titleElement.className = 'title';
        this.rootElement.appendChild(titleElement);

        if(this.parent instanceof BoxGroup) {
            this.parent.appendGroup(this.rootElement);
        }
        else {
            if(typeof this.parent === 'string') {
                this.parent = opts.root ? document.getElementById(this.parent) : document.getElementById(this.parent).childNodes[3];
            }
            this.parent.appendChild(this.rootElement);
        }
    }
    
    appendGroup(groupRoot) {
        this.rootElement.appendChild(groupRoot);
    }

    appendChild(toolBarElement) {
        toolBarElement.spawnGroup(this.rootElement);
    }

    delete() {
        this.rootElement.parentNode.removeChild(this.rootElement);
    }

}