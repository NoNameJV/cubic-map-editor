document.addEventListener('DOMContentLoaded',function() {

    // Properties 
    var minBtnInactiveOpacity = 0.25;

    // Elements 
    var toolsbar = document.getElementById('toolsbar');
    var boxElements = document.getElementsByClassName('box');

    /*
    var dragSrcEl = null;

    function swapHTMLElement(element1, element2){
        var clonedElement1 = element1.cloneNode(true);
        var clonedElement2 = element2.cloneNode(true);

        element2.parentNode.replaceChild(clonedElement1, element2);
        element1.parentNode.replaceChild(clonedElement2, element1);
    }

    function handleDragStart(e) {
        dragSrcEl = this;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
    }

    function handleDragOver(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'move'; 
        return false;
    }

    function handleDragEnter(e) {
        this.classList.add('over');
    }

    function handleDragLeave(e) {
        this.classList.remove('over');  
    }

    function handleDrop(e) {
        e.stopPropagation(); 
        if (dragSrcEl != this) {
            swapHTMLElement(dragSrcEl,this);
            this.innerHTML = e.dataTransfer.getData('text/html');
        }
        return false;
    }

    function handleDragEnd(e) {
        [].forEach.call(boxElements, function (HTMLBoxElement) {
            HTMLBoxElement.classList.remove('over');
        });
    }*/

    // Apply actions to each box!
    [].forEach.call(boxElements,function(HTMLBoxElement) {

        /*HTMLBoxElement.addEventListener('dragstart',handleDragStart,false);
        HTMLBoxElement.addEventListener('dragenter', handleDragEnter, false);
        HTMLBoxElement.addEventListener('dragover', handleDragOver, false);
        HTMLBoxElement.addEventListener('dragleave', handleDragLeave, false);
        HTMLBoxElement.addEventListener('drop', handleDrop, false);
        HTMLBoxElement.addEventListener('dragend', handleDragEnd, false);*/

        // Get elements from box!
        var titleElement    = HTMLBoxElement.childNodes[1];
        var contentElement  = HTMLBoxElement.childNodes[3];
        var BTN_Min = titleElement.getElementsByClassName('min')[0];
        var BTN_Max = titleElement.getElementsByClassName('max')[0];

        // Default opacity for BTN_Min
        BTN_Min.classList.add('disabled');

        // Apply Listener to BTN
        BTN_Min.addEventListener('click',function(event) {
            contentElement.classList.remove('show');
            BTN_Min.classList.add('disabled');
            BTN_Max.classList.remove('disabled');
        });
        
        BTN_Max.addEventListener('click',function(event) {
            contentElement.classList.add('show');
            BTN_Min.classList.remove('disabled');
            BTN_Max.classList.add('disabled');
        });
    });

});