const IInputBoxType = {
    'text': true,
    'number': true
};

class InputBox extends DefaultElementBox {

    constructor(name) {
        super();
        if(name === undefined && typeof name === 'string') {
            console.error('Please provide a valid title for InputBox');
            return;
        }
        
        this.name = name;
        this.rootElement.className = 'input'; 
        this.inputMap = new Map(); 
        var pElement = document.createElement('p');
        pElement.appendChild(document.createTextNode(name));
        this.rootElement.appendChild(pElement);

        if(this.autoSpawn)  this.spawn();
    }

    addInput(opts) {
        if(opts.type === undefined || opts.title === undefined) {
            console.error('Please provide valid type and title properties before adding input!');
            return;
        }

        var fragmentElement = document.createDocumentFragment(); 
        var type = IInputBoxType.hasOwnProperty(opts.type) ? opts.type : 'number';
        var labelElement = document.createElement('label');
        labelElement.appendChild(document.createTextNode(opts.title.toString()));
        fragmentElement.appendChild(labelElement);

        var currentValue = opts.defaultValue || ( type === 'number' ? 0 : '');
        var inputElement = document.createElement('input');
        inputElement.setAttribute('type',type);
        inputElement.setAttribute('spellcheck','false');
        inputElement.setAttribute('autocomplete','off');

        if(opts.defaultValue !== undefined) {
            inputElement.setAttribute('value',opts.defaultValue);
        }

        if(opts.onchange !== undefined && typeof opts.onchange === 'function') {
            inputElement.addEventListener('input',() => {
                setTimeout(() => {
                    if(currentValue === inputElement.value) return;
                    opts.onchange(currentValue,inputElement.value);
                    currentValue = inputElement.value;
                },700);
            });
        }

        fragmentElement.appendChild(inputElement);
        this.rootElement.appendChild(fragmentElement);
        this.inputMap.set(opts.title,inputElement); 
    }

}