
let ISelectionBoxMode = {
    'button': true,
    'select': true
};

class SelectionBox extends DefaultElementBox {

    constructor(opts) {
        super();
        if(opts.title === undefined || opts.elements === undefined) {
            console.error('Please provide title and elementsObject for addSelectionBox() function.')
            return;
        }

        if(typeof opts.title !== 'string') {
            console.error('Please provide a valid name for the selectionBox');
            return;
        }

        /*
            Define class properties
        */
        this.title      = opts.title;
        this.elements   = opts.elements;
        this.default    = opts.default || Object.keys(this.elements)[0];
        this.mode       = ISelectionBoxMode.hasOwnProperty(opts.mode) ? opts.mode : 'button';
        this.autoSpawn  = opts.autoSpawn || false;

        // Define element className
        this.rootElement.className = 'selectionBox';
        var fragmentElement = document.createDocumentFragment();
       
        // Title Elements
        {
            let titleElement = document.createElement('p');
            titleElement.innerText = opts.title;
            fragmentElement.appendChild(titleElement);
        }

        if(this.mode === 'button') {

            // Elements handler
            Object.keys(this.elements).map( ( choiceTitle ) => {
                let spanElement = document.createElement('span');
                spanElement.innerText = choiceTitle;
                let actionCallback = this.elements[choiceTitle];
                if(typeof actionCallback === 'function') {
                    spanElement.addEventListener('click',() => {
                        spanElement.blur();
                        if(!spanElement.classList.contains('selected')) {
                            actionCallback();
                            this.elements[this.default].element.classList.remove('selected');
                            spanElement.classList.add('selected');
                            this.default = choiceTitle;
                        }
                    });
                    this.elements[choiceTitle] = {
                        cb: actionCallback,
                        name: choiceTitle,
                        element: spanElement
                    };
                    fragmentElement.appendChild(spanElement);
                }
            });

            let ObjectElement = this.elements[this.default];
            if(ObjectElement !== undefined) {
                ObjectElement.cb();
                ObjectElement.element.classList.add('selected');
            }

            this.rootElement.appendChild(fragmentElement); 
        }
        else if(this.mode === 'select') {
            let selectElement = document.createElement('select');
            let elementsArray = Object.keys(this.elements); 

            elementsArray.map( ( choiceTitle ) => {
                let optionElement = document.createElement('option');
                optionElement.innerText = choiceTitle;
                optionElement.value = choiceTitle;
                let actionCallback = this.elements[choiceTitle];
                if(typeof actionCallback === 'function') {
                    optionElement.addEventListener('click',() => {
                        if(!optionElement.selected) {
                            actionCallback();
                            optionElement.selected = true;
                        }
                    });
                    this.elements[choiceTitle] = {
                        cb: actionCallback,
                        name: choiceTitle,
                        element: optionElement
                    };
                    selectElement.appendChild(optionElement);
                }
            });

            selectElement.addEventListener('change',(event) => {
                var selected = selectElement.options[selectElement.selectedIndex].value;
                this.elements[selected].cb();
            });

            let ObjectElement = this.elements[this.default];
            if(ObjectElement !== undefined) {
                ObjectElement.cb();
                ObjectElement.element.selected = true;
            }

            //selectElement.setAttribute('size',elementsArray.length);
            this.rootElement.appendChild(fragmentElement); 
            this.rootElement.appendChild(selectElement); 
        }

        if(this.autoSpawn)  this.spawn();
    }

    setupShortCut(optionName,keyName) {
        var ObjectElement = this.elements[optionName];
        if(ObjectElement !== undefined && keyName !== undefined) {
            keyName = keyName.toUpperCase();
            ObjectElement.element.innerText = ObjectElement.name+' ('+keyName+')';
            document.addEventListener('keydown',function(event) {
                if(event.key.toUpperCase() === keyName && this.default !== ObjectElement.name) {
                    ObjectElement.element.click();
                }
            });
        }
        else {
            console.error('Undefined elements action '+optionName);
        }
    }
}