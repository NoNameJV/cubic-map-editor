class ListBox extends DefaultElementBox {

    constructor(autoSpawn) {
        super();

        this.rootElement.className = 'list';
        this.list       = new Map();
        this.btn        = new Map();
        
        this.header = document.createElement('section');
        this.header.className = 'header';
        this.selectedName = null;

        this.listRoot = document.createElement('ul');
        this.rootElement.appendChild(this.header);
        this.rootElement.appendChild(this.listRoot);

        if(autoSpawn === true)  
            this.spawn();
    }

    addBTN(name,action) {
        if(name === undefined || action === undefined) {
            console.error('Please provide properties name and action for addBTN method.');
            return;
        }
        if(typeof name !== 'string') {
            return;
        }
        var btnElement = document.createElement('button');
        btnElement.appendChild(document.createTextNode(name));
        btnElement.addEventListener('click',() => {
            btnElement.blur();
            action(this);
        });
        this.btn.set(name,btnElement);
        this.header.appendChild(btnElement);
    }

    listAdd(name,callback) {
        if(name === undefined) {
            console.error('Please define a name for listAdd method');
            return;
        }
        if(this.list.has(name)) {
            console.warn(name+' is already set in this ListBox');
            return;
        }
        var liElement = document.createElement('li');
        var pElement = document.createElement('p');
        pElement.appendChild(document.createTextNode(name));
        liElement.appendChild(pElement);
        liElement.addEventListener('click',() => {
            if(this.selected === liElement) return;
            if(this.selected !== undefined) {
                this.selected.classList.remove('selected');
            }
            this.selected = liElement;
            liElement.classList.add('selected');
            this.selectedName = name;
            callback(name);
        });
        this.listRoot.appendChild(liElement);
        this.list.set(name,liElement);
    }

    listSelect(name) {
        if(this.list.has(name)) {
            var liElement = this.list.get(name);
            this.selectedName = name;
            liElement.click();
        }
    }

    listRem(name) {
        if(this.list.has(name)) {
            var liElement = this.list.get(name);
            this.listRoot.removeChild(liElement);
            this.list.delete(name);
        }
    }

}