class DefaultElementBox {

    constructor() {
        this.spawned = false;
        this.elementsMap = new Map();
        this.toolsbar = document.getElementById('toolsbar');
        this.rootElement = document.createElement('section');
    }

    spawnGroup(groupElement) {
        if(this.spawned === true) return;
        if(groupElement !== undefined) {
            groupElement.appendChild(this.rootElement);
            this.spawned = true;
        }
    }

    spawn(boxID,root) {
        if(this.spawned === true) return;
        if(boxID !== undefined) {
            var boxElement = root === true ? document.getElementById(boxID) : document.getElementById(boxID).childNodes[3];
            if(boxElement !== undefined) {
                boxElement.appendChild(this.rootElement);
                this.spawned = true;
                return;
            }
            console.warn('Failed to get boxElement that have id => '+boxID);
        }
        this.toolsbar.appendChild(this.rootElement);
        this.spawned = true;
    }

    delete() {
        this.rootElement.parentNode.removeChild(this.rootElement);
    }

}