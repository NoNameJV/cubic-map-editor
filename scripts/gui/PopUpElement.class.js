/**
 * Component to display PopUp
 * 
 * @class PopUpElement
 * @extends {HTMLElement}
 */
class PopUpElement extends HTMLElement {
  /**
   * Creates an instance of PopUpElement.
   * 
   * 
   * @memberOf PopUpElement
   */
  constructor() {
    super();
    this.shadow = this.attachShadow({mode:'open'});
  }

  /**
   * 
   * 
   * @param {any} dom
   * @returns
   * 
   * @memberOf PopUpElement
   */
  attachDom(dom) {
    while (this.shadow.hasChildNodes())
      this.shadow.removeChild(this.shadow.firstChild);

    return this.shadow.appendChild(dom);
  }
}

customElements.define('pop-up', PopUpElement);