class ButtonBox extends DefaultElementBox {

    constructor(opts) {
        super();
        if(opts.buttonTitle === undefined || opts.callback === undefined) {
            console.error('Please define a buttonTitle & callback properties for ButtonBox!');
            return;
        }

        this.rootElement.className = 'button';
        var fragmentElement = document.createDocumentFragment();

        if(opts.name !== undefined && typeof opts.name === 'string') {
            var pElement = document.createElement('p');
            pElement.innerText = opts.name;
            this.elementsMap.set('title',pElement);
            fragmentElement.appendChild(pElement);
        }

        if(typeof opts.buttonTitle === 'string' && typeof opts.callback === 'function') {
            var buttonElement = document.createElement('button');
            buttonElement.innerHTML = opts.buttonTitle.toString();
            buttonElement.addEventListener('click',opts.callback);
            this.elementsMap.set('button',buttonElement);
            fragmentElement.appendChild(buttonElement);
            
            function callback(event) {
                buttonElement.blur();
                opts.callback(buttonElement);
            }
        }
        else {
            console.error('Invalid type from buttonTitle or callback properties!');
            return;
        }

        this.rootElement.appendChild(fragmentElement);
        this.shortcut = false;

        if(this.autoSpawn)  this.spawn();
    }

    setupShortCut(keyName) {
        if(this.shortcut) return;
        if(keyName !== undefined) {
            keyName = keyName.toUpperCase();
            const buttonElement = this.elementsMap.get('button');
            buttonElement.innerHTML = buttonElement.innerHTML+' ('+keyName+')';
            document.addEventListener('keydown',function(event) {
                if(event.key.toUpperCase() === keyName) {
                    buttonElement.click();
                }
            });
            this.shortcut = true;
        }
        else {
            console.error('Undefined elements action '+optionName);
        }
    }

}