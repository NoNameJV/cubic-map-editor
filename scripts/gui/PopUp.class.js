/**
 * @example
 
// choices params examples
new PopUp({
  choices: {
    type: 'radio',
    list: [
      {label: 'foo', value: 'foo'},
      {label: 'bar', value: 'bar'},
      {label: 'baz', value: 'baz'},
    ]
  }
}).getInput()
  .then(value => {
    console.log(value);

    return new PopUp({
      choices: {
        type: 'checkbox',
        list: [
          {label: 'foo', value: 'foo'},
          {label: 'bar', value: 'bar'},
          {label: 'baz', value: 'baz'},
        ]
      }
    }).getInput()
  })
  .then(value => {
    console.log(value);

    return new PopUp({
      choices: {
        type: 'select',
        list: [
          {label: 'foo', value: 'foo'},
          {label: 'bar', value: 'bar'},
          {label: 'baz', value: 'baz'},
        ]
      }
    }).getInput()
  })
  .then(value => {
    console.log(value);

    return new PopUp({
      choices: {
        type: 'select',
        multiple: true,
        list: [
          {label: 'foo', value: 'foo'},
          {label: 'bar', value: 'bar'},
          {label: 'baz', value: 'baz'},
        ]
      }
    }).getInput()
  })
  .then(value => console.log(value))

 */

/**
 * Util class for instantiate PopUp in js code
 * 
 * @class PopUp
 */
class PopUp {

  /**
   * Creates an instance of PopUp.
   * @param {any} [{
   *     confirm={
   *       label:'Confirmer',
   *       value:true
   *     },
   *     cancel={
   *       label:'Annuler',
   *       value: false
   *     },
   *     title='Modal pop-up',
   *     description='Description',
   *     target=document.body, // Element
   *     choices={ // false if not defined
   *       label: 'Choices',
   *       name: 'choices', // for name attribute
   *       multiple: false, // true, false
   *       type: 'select', // 'select, checkbox, radio'
   *       list: [] // {label, value}
   *     },
   *     input={ // false if not defined
   *       label: 'Input',
   *       name: 'input', // for name attribute
   *       type: 'text', // anything can go in input type (except checkbox and radio, use choices param instead)
   *       value: null
   *     }
   *   }={}] 
   * 
   * @memberOf PopUp
   */
  constructor({
    confirm={
      label:'Confirmer',
      value:true
    },
    cancel={
      label:'Annuler',
      value: false
    },
    title='Modal pop-up',
    description='Description',
    target=document.body, // Element
    choices=false,
    input=false
  }={}) {
    Object.assign(this, {confirm, cancel, target, choices, input, title, description});
    this.init()
  }

  /**
   * part of constructor
   * populate template
   * instantiate PopUpElement
   * attach populate template to PopUpElement
   * 
   * @memberOf PopUp
   */
  init() {
    const tpl = document.getElementById('tpl-pop-up').content.cloneNode(true);

    // PopUp Title
    tpl.querySelector('#pop-up-title').textContent = this.title;
    // PopUp Description
    tpl.querySelector('#pop-up-description').textContent = this.description;

    // PopUp Cancel Button
    const $cancel = tpl.querySelector('#pop-up-cancel');
    $cancel.textContent = this.cancel.label;
    $cancel.value = this.cancel.value;
    
    // PopUp Confirm Button
    const $confirm = tpl.querySelector('#pop-up-confirm');
    $confirm.textContent = this.confirm.label;
    $confirm.value = this.confirm.value;

    const $form = tpl.querySelector('#pop-up-form');

    // choices
     this._attachChoices($form);

    // input
    this._attachInput($form);

    const $popUp = new PopUpElement();
    $popUp.attachDom(tpl);

    this.target.appendChild($popUp);

    this.popUpElement = $popUp;
    this.tpl = this.popUpElement.shadow.querySelector('section'); // gros point de fragilité mais avec this.tpl = clone, j'avais un document-fragment vide :/
  }

  /**
   * populate form pop-up with choices
   * 
   * @param {Element} $form
   * 
   * @memberOf PopUp
   */
  _attachChoices($form) {
    if (!this.choices) return;

    const choices = Object.assign({
      label: 'Choices',
      name: 'choices', // for name attribute
      multiple: false, // true, false
      type: 'select', // 'select, checkbox, radio'
      list: [] // {label, value}
    }, this.choices);
    if (choices.list ? choices.list.length : false) {
      const isCheckboxOrRadio = choices.type == 'checkbox' || choices.type == 'radio';
      const multiple = choices.multiple || choices.type == 'checkbox';

      const label = document.createElement('strong');
      label.innerHTML = choices.label;
      $form.appendChild(label);

      if (choices.type == 'checkbox' || choices.type == 'radio') {
        const choice = document.createElement('ul');
        
        for (let {label, value} of choices.list) {
          const li = document.createElement('li');
          const labelNode = document.createElement('label');
          const item = document.createElement('input');
          const text = document.createTextNode(` ${label}`);
          item.value = value;
          item.setAttribute('type', choices.type);
          item.setAttribute('name', `${choices.name}`);

          li.appendChild(labelNode);
          labelNode.appendChild(item);
          labelNode.appendChild(text);
          choice.appendChild(li);
        }

        $form.appendChild(choice);
      }
      else {
        const choice = document.createElement('select');
        choice.setAttribute('name', `${choices.name}`);
        if (multiple) {
          choice.setAttribute('multiple', 'multiple');
        }
        
        for (let {label, value} of choices.list) {
          const item = document.createElement('option');
          item.textContent = label;
          item.value = value;

          choice.appendChild(item);
        }

        $form.appendChild(choice);
      }
    }
  }

  /**
   * populate form with input
   * 
   * @param {Element} $form 
   * @returns 
   * 
   * @memberOf PopUp
   */
  _attachInput($form) {
    if (!this.input) return;

    const input = Object.assign({
      label: 'Input',
      name: 'input', // for name attribute
      type: 'text', // anything can go in input type (except checkbox and radio, use choices param instead)
      value: null
    }, this.input);

    const id = Math.floor(Math.random() * 100000);

    const label = document.createElement('label');
    label.setAttribute('for', `${id}`);
    label.innerHTML = input.label;

    const inputstr =
      `<input type="${input.type || 'text'}"
              name="${input.name || 'input'}"
              value="${input.value || ''}"
              id="${id}" />`
    
    $form.appendChild(label);
    $form.insertAdjacentHTML('beforeend', inputstr);
  }

  _getDataFromForm(form) {
    const formdata = new FormData(form);

    return [...formdata.keys()].reduce((prev, key) => {
      const inputs = form.querySelectorAll(`select[name="${key}"], input[type=checkbox][name="${key}"]`);
      const multiple = (inputs.length > 1) || (inputs[0] ? inputs[0].hasAttribute('multiple') : false);
      
      const value = multiple ? formdata.getAll(key) : formdata.get(key);

      return prev.set(key, value)
    }, new Map());
  }

  /**
   * Attach event to button and await click
   * confirm => resolve
   * cancel => reject 
   * 
   * In resolve and reject promise is given the box clicked and inputs in PopUp
   * 
   * @returns {Promise}
   * 
   * @memberOf PopUp
   */
  getInput() {
    return new Promise((resolve, reject) => {
      const form = this.tpl.querySelector('#pop-up-form');

      Array.from(this.tpl.querySelectorAll('.pop-up-cancel')).forEach((element) => {
        element.addEventListener('click', (event) => {
          this.target.removeChild(this.popUpElement);

          // http://stackoverflow.com/questions/2276463/how-can-i-get-form-data-with-javascript-jquery
          reject({
            cancel: this.cancel,
            data: this._getDataFromForm(form)
          });
        });
      });

      this.tpl.querySelector('#pop-up-confirm').addEventListener('click', (element) => {
          this.target.removeChild(this.popUpElement);

          const data = new FormData(form);
          resolve({
            confirm: this.confirm,
            data: this._getDataFromForm(form)
          });
      });
    });
  }
}