class Editor extends THREE.Scene {

	constructor(cameraControl, domElement) {
		super();

		this.domElement = domElement;
		this.cameraControl = cameraControl;

		this.mouseDown = false;

		this.layers = new Map();
		this.layers.set("Default", new Layer());
		this.currentLayer = this.layers.get("Default");

		this.timer = 0;
		this.mouse = new THREE.Vector2();
		this.rayCaster = new THREE.Raycaster();
		this.intersectObjects = [];
		this.lastIntersect = null;

		this.RATIO = 5;
		this.currentSize = 5;
		this.currentTexture = "textures/devCube.png";

		this.setMeshes();
		
		// bindings

		Editor._onMouseUp = bind(this, this.onMouseUp);
		Editor._onMouseDown = bind(this, this.onMouseDown);
		Editor._onMouseMove = bind(this, this.onMouseMove);

		this.domElement.addEventListener('mouseup', Editor._onMouseUp, false);
		this.domElement.addEventListener('mousedown', Editor._onMouseDown, false);
		this.domElement.addEventListener('mousemove', Editor._onMouseMove, false);
	}

	setMeshes() {

		// Set rollOver block

		this.rollOverSize = new THREE.Vector3(this.currentSize);
		this.rollOverGeometry = new THREE.BoxGeometry(this.currentSize, this.currentSize, this.currentSize);
		this.rollOverGeometry = new THREE.EdgesGeometry(this.rollOverGeometry);
		this.rollOverMaterial = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 100 });
		this.rollOver = new THREE.LineSegments( this.rollOverGeometry, this.rollOverMaterial )
		this.rollOver.renderOrder = 0;
		this.rollOver.position.set(0, 0, 0);

		var planegeometry = new THREE.PlaneBufferGeometry(1000, 1000);
		var planeforwardgeometry = new THREE.BoxGeometry(1000, 1000, 1);
		planegeometry.rotateX(-Math.PI/2);

		this.plane = new THREE.Mesh(planegeometry, new THREE.MeshBasicMaterial({color: 0x555555, visible: false }));
		this.planeForward = new THREE.Mesh(planeforwardgeometry, new THREE.MeshBasicMaterial({color: 0x555555, visible: false }));
		this.planeForward.position.z = -100;

		// add grid to scene

		const size = this.RATIO*100, step = this.RATIO;
		const geometry = new THREE.Geometry();
		for (var i = -size; i <= size; i += step) {
			geometry.vertices.push(new THREE.Vector3(-size, 0, i));
			geometry.vertices.push(new THREE.Vector3(size, 0, i));

			geometry.vertices.push(new THREE.Vector3(i, 0, -size));
			geometry.vertices.push(new THREE.Vector3(i, 0, size));
		}
		const material = new THREE.LineBasicMaterial({color: 0xffffff, opacity: 0.2, transparent: true });
		const line = new THREE.LineSegments(geometry, material);

		// add components
		
		this.add(line);
		this.add(this.cameraControl.yawObject);
		this.add(this.rollOver);
		this.add(this.planeForward);

		this.layers.forEach(layer => this.add(layer));
		
		THREE.SceneUtils.attach(this.planeForward, this, this.cameraControl.camera);

		this.intersectObjects.push(this.plane);
		this.intersectObjects.push(this.planeForward);
	}

	onMouseUp(mouseEvent) {
		mouseEvent.preventDefault();
		this.mouseDown = false;
	}

	onMouseDown(mouseEvent) {
		mouseEvent.preventDefault();

		this.mouse.set((mouseEvent.clientX / window.innerWidth) * 2 - 1, -(mouseEvent.clientY / window.innerHeight) * 2 + 1);
		this.rayCaster.setFromCamera(this.mouse, this.cameraControl.camera);

		const intersects = this.rayCaster.intersectObjects(this.intersectObjects);
		if(intersects.length > 0) {
			const intersect = intersects[0];
			
			if(mouseEvent.button === 0) {
				this.mouseDown = true;
				var voxel = this.addVoxelToIntersect(intersect, this.currentSize, this.currentTexture);
			}
			else if(mouseEvent.button === 1) {
				this.rollOver.visible = false;
			}
			else if(mouseEvent.button === 2) {
				if(intersect.object != this.plane && intersect.object != this.planeForward) {
					this.currentLayer.remove(intersect.object);
					this.intersectObjects.splice(this.intersectObjects.indexOf(intersect.object), 1);
				}
			}
		}
	}

	onMouseMove(mouseEvent) {
		if(!this.cameraControl.pointerLockElement) {
			this.rollOver.visible = true;
			this.timer += 1;
			
			this.mouse.set((mouseEvent.clientX / window.innerWidth) * 2 - 1, -(mouseEvent.clientY / window.innerHeight) * 2 + 1);
			this.rayCaster.setFromCamera(this.mouse, this.cameraControl.camera);

			const intersects = this.rayCaster.intersectObjects(this.intersectObjects);
			if(intersects.length > 0) {
				const intersect = intersects[0];

				const isPlaneIntersect = (intersect.object != this.plane && intersect.object != this.planeForward);
				
				if(isPlaneIntersect && this.cameraControl.camera.position.y >= 0) this.rollOver.position.copy(intersect.point).sub(intersect.face.normal);
				else if(isPlaneIntersect && this.cameraControl.camera.position.y < 0) this.rollOver.position.copy(intersect.point).add(intersect.face.normal);
				else this.rollOver.position.copy(intersect.point);

				this.rollOver.position.divideScalar(5).floor().multiplyScalar(5).addScalar(2.5);

				if(this.mouseDown) {
					if(intersect.object !== this.lastIntersect) {
						var voxel = this.addVoxelToIntersect(intersect, this.currentSize, this.currentTexture);
						this.lastIntersect = voxel;

						this.timer = 0;
					}
				}
			}
		}
		else {
			this.rollOver.visible = false;
		}
	}

	addVoxelToIntersect(intersect, size, texture) {
		var position = intersect.point
			.add(intersect.face.normal)
			.divideScalar(this.RATIO)
			.floor()
			.multiplyScalar(this.RATIO)
			.addScalar(this.RATIO/2);
		
		var voxel = this.createVoxel(size, texture, position);
		return voxel;
	}

	createVoxel(size = 5, texture, position, orientation = new THREE.Vector3(0)) {
		const voxel = new Block(size, texture);
		voxel.position.copy(position);
		voxel.rotation.copy(orientation);

		this.currentLayer.add(voxel);
		this.intersectObjects.push(voxel);

		return voxel;
	}

	reset() {
		this.layers.forEach(layer => layer.children = []);
		this.intersectObjects = [];
		this.intersectObjects.push(this.plane);
		this.intersectObjects.push(this.planeForward);
	}

	addLayer(name, current = false) {
        this.layers.set(name, new Layer());
		this.add(this.layers.get(name));
        if(current) this.currentLayer = this.layers.get(name);
	}

	removeLayer(name, newname) {
		this.layers.get(name).children.forEach(value => this.intersectObjects.splice(this.intersectObjects.indexOf(value), 1));
		this.layers.get(name).children = [];
		this.layers.delete(name);
		if(newname) this.currentLayer = this.layers.get(newname);
	}

	setIntersect(intersects) {
		this.intersectObjects = intersects;
	}

	update(delta) {
  		this.cameraControl.update(delta);
		this.cameraControl.camera.updateMatrixWorld(true);

		this.planeForward.position.z = -150;
	}

	setRollOverSize(x, y, z) {
		this.rollOverGeometry.x = x;
		this.rollOverGeometry.y = y;
		this.rollOverGeometry.z = z;
	}

}

Editor._onMouseDown;
Editor._onMouseMove;