const keyboard = {
    "qwerty": {
        "Forward": "w",
        "Backward": "s",
        "Up": " ",
        "Down": "shift",
        "Left": "a",
        "Right": "d"
    },

    "azerty": {
        "Forward": "z",
        "Backward": "s",
        "Up": " ",
        "Down": "shift",
        "Left": "q",
        "Right": "d"
    }
};

class CameraControls {

    /**
     * @desc constructor of the camera & its controls
     * @param {Object} options - the options of the controls and camera 
     * @param {Element} domElement - document by default
     **/
    constructor(options = CameraControls.defaultOptions, domElement = document) {

        // initialize properties

        this.options = options;
        this.mouseLayer = document.getElementById('mouseLayer');
        this.domElement = domElement;

        this.enabled = true;

        this.mouse = new THREE.Vector2();
        this.angleX = 0;
        this.angleY = 0;

        this.viewHalfX = 0;
        this.viewHalfY = 0;

        this.rotationSpeedX = 0.002;
        this.rotationSpeedY = 0.004;

        this.moveMouse = false;
        this.moveUp = false;
        this.moveDown = false;
        this.moveLeft = false;
        this.moveRight = false;
        this.moveForward = false;
        this.moveBackward = false;

        this.keyboard = keyboard[options.keyboard];

        this.pitchObject = new THREE.Object3D();
        this.yawObject = new THREE.Object3D();

        this.yawObject.position.y = 10;

        this.camera = new THREE.PerspectiveCamera(
            this.options.view_angle,
            this.options.aspect,
            this.options.near,
            this.options.far
        );

        this.camera.rotation.set(0, 0, 0);
        this.camera.position.set(this.options.position.x, this.options.position.y, this.options.position.z);

        this.pitchObject.add(this.camera);
        this.yawObject.add(this.pitchObject);

        // Add mouse locker

        const blocker = document.getElementById("blocker");
        const havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

        this.mouseLocked = false;
        this.pointerLockElement = (document.pointerLockElement === document.body || document.mozPointerLockElement === document.body || document.webkitPointerLockElement === document.body);

        if (this.havePointerLock) {
            var pointerlockchange = pointerEvent => {
                if (this.pointerLockElement) {
                    blocker.style.display = 'none';
                    this.mouseLocked = true;
                } else {
                    blocker.style.display = '-webkit-box';
                    blocker.style.display = '-moz-box';
                    blocker.style.display = 'box';
                    this.mouseLocked = false;
                }
            }
            var pointerlockerror = pointerEvent => {
                console.log("Error : pointerlockerror");
            }
        }

        // initialize & add eventListener bindings

        CameraControls._onMouseMove = bind(this, this.onMouseMove);
        CameraControls._onMouseDown = bind(this, this.onMouseDown);
        CameraControls._onMouseUp = bind(this, this.onMouseUp);
        CameraControls._onKeyDown = bind(this, this.onKeyDown);
        CameraControls._onKeyUp = bind(this, this.onKeyUp);

        this.domElement.addEventListener('contextmenu', contextmenu, false);
        this.domElement.addEventListener('mousedown', CameraControls._onMouseDown, false);
        this.domElement.addEventListener('mousemove', CameraControls._onMouseMove, false);
        this.domElement.addEventListener('mouseup', CameraControls._onMouseUp, false);

        this.domElement.addEventListener('keydown', CameraControls._onKeyDown, false);
        this.domElement.addEventListener('keyup', CameraControls._onKeyUp, false);

        this.mouseLayer.addEventListener('pointerlockchange', pointerlockchange, false);
        this.mouseLayer.addEventListener('mozpointerlockchange', pointerlockchange, false);
        this.mouseLayer.addEventListener('webkitpointerlockchange', pointerlockchange, false);

        this.mouseLayer.addEventListener('pointerlockerror', pointerlockerror, false);
        this.mouseLayer.addEventListener('mozpointerlockerror', pointerlockerror, false);
        this.mouseLayer.addEventListener('webkitpointerlockerror', pointerlockerror, false);
    }

    handleResize() {
        if (this.domElement === document) {
            this.viewHalfX = window.innerWidth / 2;
            this.viewHalfY = window.innerHeight / 2;
        } else {
            this.viewHalfX = this.domElement.offsetWidth / 2;
            this.viewHalfY = this.domElement.offsetHeight / 2;
        }
    }

    onMouseDown(mouseEvent) {
        if (mouseEvent.button === 1) {
            document.body.requestPointerLock = document.body.requestPointerLock || document.body.mozRequestPointerLock || document.body.webkitRequestPointerLock;
            document.body.requestPointerLock();
            this.mouseLocked = true;
        }
    }

    onMouseUp(mouseEvent) {
        if (mouseEvent.button === 1) {
            document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock;
            document.exitPointerLock();
            this.mouseLocked = false;
        }
    }

    onMouseMove(mouseEvent) {
        this.pointerLockElement = (document.pointerLockElement === document.body || document.mozPointerLockElement === document.body || document.webkitPointerLockElement === document.body);
        if (!this.enabled || !this.pointerLockElement) return;

        this.moveMouse = true;
        this.mouse.x = mouseEvent.movementX || mouseEvent.mozMovementX || mouseEvent.webkitMovementX || 0;
        this.mouse.y = mouseEvent.movementY || mouseEvent.mozMovementY || mouseEvent.webkitMovementY || 0;

        this.yawObject.rotation.y -= this.mouse.x * this.rotationSpeedY;
        this.pitchObject.rotation.x -= this.mouse.y * this.rotationSpeedX;

        this.pitchObject.rotation.x = THREE.Math.clamp(this.pitchObject.rotation.x, -Math.PI, Math.PI);
    }

    onKeyDown(keyEvent) {
        switch (keyEvent.key.toLowerCase()) {
            case this.keyboard["Forward"]:
                this.moveForward = true;
                break;
            case this.keyboard["Backward"]:
                this.moveBackward = true;
                break;
            case this.keyboard["Up"]:
                this.moveUp = true;
                break;
            case this.keyboard["Down"]:
                this.moveDown = true;
                break;
            case this.keyboard["Left"]:
                this.moveLeft = true;
                break;
            case this.keyboard["Right"]:
                this.moveRight = true;
                break;
        }
    }

    onKeyUp(keyEvent) {
        switch (keyEvent.key.toLowerCase()) {
            case this.keyboard["Forward"]:
                this.moveForward = false;
                break;
            case this.keyboard["Backward"]:
                this.moveBackward = false;
                break;
            case this.keyboard["Up"]:
                this.moveUp = false;
                break;
            case this.keyboard["Down"]:
                this.moveDown = false;
                break;
            case this.keyboard["Left"]:
                this.moveLeft = false;
                break;
            case this.keyboard["Right"]:
                this.moveRight = false;
                break;
        }
    }

    // Get the PerspectiveCamera ThreeJS component 
    getCamera() {
        return this.camera;
    }

    // Update method requesting each frame with requestAnimationFrame
    update(delta) {
        if (this.enabled === false) return;

        // Normalized movement using Math.cos & Math.sin

        const actualSpeed = delta * this.options.moveSpeed;

        if (this.moveForward) this.yawObject.translateZ(-actualSpeed);
        else if (this.moveBackward) this.yawObject.translateZ(actualSpeed);

        if (this.moveLeft) this.yawObject.translateX(-actualSpeed);
        else if (this.moveRight) this.yawObject.translateX(actualSpeed);

        if (this.moveUp) this.yawObject.position.y += (actualSpeed);
        else if (this.moveDown) this.yawObject.position.y += (-actualSpeed);
    }

    // Dispose all eventEmitter set to the domElement linked to the camera
    dispose() {
        CameraControls._onMouseMove = bind(this, this.onMouseMove);
        CameraControls._onMouseDown = bind(this, this.onMouseDown);
        CameraControls._onMouseUp = bind(this, this.onMouseUp);
        CameraControls._onKeyDown = bind(this, this.onKeyDown);
        CameraControls._onKeyUp = bind(this, this.onKeyUp);

        this.domElement.removeEventListener('contextmenu', contextmenu, false);
        this.domElement.removeEventListener('mousedown', CameraControls._onMouseDown, false);
        this.domElement.removeEventListener('mousemove', CameraControls._onMouseMove, false);
        this.domElement.removeEventListener('mouseup', CameraControls._onMouseUp, false);

        this.domElement.removeEventListener('keydown', CameraControls._onKeyDown, false);
        this.domElement.removeEventListener('keyup', CameraControls._onKeyUp, false);
    }

    setKeyboard(newKeyboard) {
        this.options.keyboard = newKeyboard;
        this.keyboard = keyboard[this.options.keyboard];
    }
}

// Default options for the camera parameters
CameraControls.defaultOptions = {
    view_angle: 45,
    near: 1,
    far: 10000,
    moveSpeed: 100.0,
    position: new THREE.Vector3(0, 10, 0),
    aspect: window.innerWidth / window.innerHeight,
    keyboard: "azerty"
}

// Binding variables of the eventListeners below
CameraControls._onMouseMove;
CameraControls._onMouseDow;
CameraControls._onMouseUp
CameraControls._onKeyDown
CameraControls._onKeyUp;