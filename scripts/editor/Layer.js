class Layer extends THREE.Object3D {

    constructor() {
        super();

        this.merged = false;
        this.visible = true;

        this._data = [];
    }

    clear() {
        this._data.forEach(block => editor.remove(block));
    }

    setVisible(visible) {
        this.visible = visible;
        this.traverse(obj => obj.visible = visible);
    }
}