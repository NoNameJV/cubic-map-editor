const path = require('path');

class Block extends THREE.Mesh {

	constructor(size, texture, color) {
		super();

		this.geometry = new THREE.BoxGeometry(size/Block.RATIO, size/Block.RATIO, size/Block.RATIO);
		this.texture = texture;
		this.color = color;
		let texturePath = path.join( __dirname , "../static/images/", this.texture );
		// console.log(texturePath);
		this.material = new THREE.MeshBasicMaterial({
			color: this.color || null,
			map: (typeof this.texture === "string") ?
				new THREE.TextureLoader().load(texturePath) : this.texture
		});
	}

}

Block.RATIO = 1;