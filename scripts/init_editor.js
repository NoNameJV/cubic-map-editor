// Define constants
const Worker = new WorkerTask();
const clock = new THREE.Clock();
const _ = undefined;

const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const renderer = new THREE.WebGLRenderer();
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

const mouseLayer = document.getElementById("mouseLayer");

const camera = new CameraControls(_, document);
const editor = new Editor(camera, mouseLayer);

// Fondamental functions 

function update() {
    renderer.render(editor, camera.camera);
    renderer.setSize(window.innerWidth, window.innerHeight);
    editor.update(clock.getDelta());

    requestAnimationFrame(update);
}
requestAnimationFrame(update);

function onWindowResize() {
    renderer.setSize(window.innerWidth, window.innerHeight);

    camera.camera.aspect = window.innerWidth / window.innerHeight;
    camera.camera.updateProjectionMatrix();
    camera.handleResize();
}
window.addEventListener('resize', onWindowResize, false);

// Define Actions
const Actions = {
    "dupplicate": new Action("dupplicate", intersects => {
        const uniqDouble = editor.intersectObjects.filter((value, index) => {
            var UIndex = -1;
            for (let i = 0; i < editor.intersectObjects.length; i++) {
                if (editor.intersectObjects[i].position.equals(value.position)) {
                    UIndex = i
                }
            }
            return UIndex !== index;
        });

        for (let i = 0; i < uniqDouble.length; i++) {
            editor.currentLayer.remove(uniqDouble[i]);
            editor.intersectObjects.splice(editor.intersectObjects.indexOf(uniqDouble[i]), 1);
        }
    }),
    "changeBlock": new Action("change block", _ => {
        switch (editor.currentTexture) {
            case "textures/devCube.png":
                editor.currentTexture = "textures/devCube3.png";
                break;
            case "textures/devCube2.png":
                editor.currentTexture = "textures/devCube.png";
                break;
            case "textures/devCube3.png":
                editor.currentTexture = "textures/devCube2.png";
                break;
        }
    })
};

// Add & run task to the Worker

Worker.addTask(Actions.dupplicate, 50);
Worker.addTask(Actions.changeBlock, 100);

Worker.run("dupplicate", editor.intersectObjects);
Worker.run("change block", editor.intersectObjects);