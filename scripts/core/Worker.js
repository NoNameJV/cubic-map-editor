class WorkerTask {

	constructor() {
		this.tasks = new Map();
	}

	run(name = undefined) {
		if(name === undefined || name === null) {
			for(let [key, task] of this.tasks) {
				const args = (task.arguments.length > 0) ? task.arguments : [].slice.call(arguments, 1);

				if(!isNaN(task.interval)) setInterval(task.run, task.interval, ...args);
				else task.run.apply(this, args);
			}
		}
		else {
			const task = this.tasks.get(name);
			const args = (task.arguments !== undefined) ? ((task.arguments.length > 0) ? task.arguments : [].slice.call(arguments, 1)) : [];
			
			if(task.interval != null && typeof task.interval === "number") {
				setInterval(task.run, task.interval, ...args);
			}
			else {
				task.run.apply(this, args);
			}
		}
	}

	addTasks(actions, interval = null) {
		for(let [key, action] of actions) this.addTask(action, interval);
	}

	addTask(action, interval = null) {
		const task = {
			name: action.name,
			run: action.callback,
			arguments: action.arguments || [].slice.call(arguments, 3) || [],
			interval: interval || undefined
		}
		this.tasks.set(action.name, task);
	}

	getTask(name) {
		return this.tasks.get(name);
	}
}

class Action {
	constructor(name, callback) {
		this.name = name;
		this.callback = callback;
	}

	set(...args) {
		this.arguments = args;
	}
}