document.addEventListener('DOMContentLoaded',function() {

    let CameraBox = new SelectionBox({
        title: "Camera Speed",
        default: "Slow",
        mode: 'select',
        elements: {
            "Slow": function() {
                console.log("Slow mode selected");
            },
            "Fast": function() {
                console.log("Fast mode selected")
            }
        }
    });

    let resetBTN = new ButtonBox({
        name: "Reset Map",
        buttonTitle: "Reset",
        callback: function() {
            console.log("Reset Map!")
            editor.reset();
        }
    });
    resetBTN.spawn('box_settings');

    let testBTN = new ButtonBox({
        name: "JSON Map",
        buttonTitle: "Generate",
        callback: function() {
            console.log("Generate Map!")
        }
    });
    testBTN.spawn('box_settings');

    let CubeSize = new InputBox('Cube size');
    CubeSize.addInput({
        title: 'X',
        type: 'number',
        defaultValue: 32,
        onchange: function(oldValue,newValue) {
            console.log('Input X changed! old => '+oldValue+' new => '+newValue);
        }
    });
    CubeSize.addInput({
        title: 'Y',
        type: 'number',
        defaultValue: 32,
        onchange: function(oldValue,newValue) {
            console.log('Input Y changed! old => '+oldValue+' new => '+newValue);
        }
    });
    CubeSize.spawn('box_settings');

    CameraBox.setupShortCut("Slow",'-');
    CameraBox.setupShortCut("Fast",'+');
    CameraBox.spawn('box_settings');

    let keyboardElems = {};
    for(let elemKey in keyboard)
        keyboardElems[elemKey] = function(){
            console.log(elemKey)
            camera.setKeyboard(elemKey)
        }

    let SelectKeyboardBox = new SelectionBox({
        title: "Keyboard Map",
        default: "azerty",
        mode: "select",
        elements: keyboardElems
    });
    SelectKeyboardBox.spawn('box_settings');

    let ListB = new ListBox();

    ListB.addBTN('add',function(self) {
        console.log('add clicked');

        const name = "Layer"+THREE.Math.randInt(0, 1000);
        editor.addLayer(name, true);

        self.listAdd(name, name => editor.currentLayer = editor.layers.get(name));
        self.listSelect(name);
    });

    ListB.addBTN("clear",function(self) {
        editor.layers.get(self.selectedName).children.forEach(value => editor.intersectObjects.splice(editor.intersectObjects.indexOf(value), 1));
        editor.layers.get(self.selectedName).children = [];
    });

    ListB.addBTN("delete", function(self) {
        if(self.selectedName !== "Default") {
            self.listRem(self.selectedName);
            editor.removeLayer(self.selectedName);
            self.listSelect('Default');
        }
        else {
            self.btn.get('clear').click();
        }
    });

    ListB.addBTN('merge',function(self) {
        console.log('merge clicked');
        /*const selected = self.listSelect();

        editor.layers.get(selected.get("previous")).children.forEach(value => {
            editor.layers.get(selected.get("name")).add(value);
        })
        editor.removeLayer(selected.get("previous"));
        self.listRem(selected.get("previous"));
        */
    });

    ListB.listAdd('Default', name => editor.currentLayer = editor.layers.get(name));
    ListB.listSelect("Default");
    ListB.spawn('box_layers');

    let visibleLayerBTN = new ButtonBox({
        name: "Visible",
        buttonTitle: "true",
        callback: function(button) {
            const visible = !editor.currentLayer.visible;
            editor.currentLayer.setVisible(visible);
            button.textContent = visible;

            console.log("Set visible to : "+visible+"!");
        }
    });
    visibleLayerBTN.spawn('box_layers'); 

});