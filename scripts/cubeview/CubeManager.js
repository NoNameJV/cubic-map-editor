let cubeViewRenderer,cubeViewManager; 

document.addEventListener('DOMContentLoaded',(event) => {

    // Require NodeJS Packages
    const path          = require('path');
    const fs            = require('fs');
    const {dialog}      = require('electron').remote
    const events        = require('events');

    /*
        Create local Manager class to manage CubeView interfaces & local write disk.
    */
    class Manager extends events {

        constructor() {
            super(); 
            this.initialized    = false;
            this.projectPath    = path.join( __dirname , '../' );
            this.tileData       = null;

            // Require button from .html
            this.HTMLButton_addCube        = document.getElementById('viewcube_addcube');
            this.HTMLButton_deleteCube     = document.getElementById('viewcube_deletecube');
            this.HTMLButton_loadTileSet    = document.getElementById('viewcube_loadtileset');
            this.HTMLButton_createTileSet  = document.getElementById('viewcube_createtileset');
            this.HTMLRootSection           = document.getElementById('viewcube');
        }

        /*
            CREATE DIRECTORY
        */
        createDirectory(path) {
            return new Promise( (resolve,reject) => {
                fs.exists( path , (exists) => {
                    if(exists === false) {
                        fs.mkdir( path , (err) => {
                            if(err) reject(err);
                            resolve(true);
                        });
                    }
                    else {
                        resolve(false);
                    }
                })
            });
        }

        /*
            INITIALIZE NEW TILESET!
        */
        init(tileName) {

            if(tileName === undefined) {
                throw new Error('Please define tileName arguments for cubeView_init method');
            }
            this.initialized = false;

            // Create default directory structure!
            const assetsDirectory   = path.join( this.projectPath , 'assets' );
            const tileSetsDirectory = path.join( assetsDirectory , 'tilesets' );

            // TODO : Better async handle!
            this.createDirectory(assetsDirectory)
            .then( this.createDirectory(tileSetsDirectory) )
            .then( _ => {

                const tilePath = path.join( tileSetsDirectory , tileName );
                fs.exists( tilePath , (exists) => {
                    if(exists) {
                        const manifestFile = path.join( tilePath , 'manifest.json' ); 
                        fs.readFile(manifestFile,(err,buffer) => {
                            try {
                                this.tileData = JSON.parse(buffer.toString());
                            }
                            catch(exceptionErr) {
                                throw new Error(exceptionErr);
                            }
                            this.tileData.path = tilePath;
                            this.tileData.texturesPath = path.join( tilePath, 'textures' );
                            this.createDirectory(this.tileData.texturesPath)
                            .then( _=> {
                                this.emit('initRenderer',this.tileData);
                            } );
                        });
                    }
                    else {
                        console.warn(`Tile set ${tileName} does not exist in the directory ${tileSetsDirectory}`);
                    }
                });
            })
            .catch( err => {
                console.error(err);
            });

        }

        cubeViewCreate(name) {
            if(name === undefined) {
                throw new Error('Please define name arguments for cubeViewCreate method');
            }

            return new Promise( (resolve,reject) => {
                // Create default directory structure!
                const assetsDirectory   = path.join( this.projectPath , 'assets' );
                const tileSetsDirectory = path.join( assetsDirectory , 'tilesets' );

                this.createDirectory(assetsDirectory)
                .then( this.createDirectory(tileSetsDirectory) )
                .then( _ => {

                    const ManifestObject = {
                        name,
                        width: 32,
                        height: 32,
                        texture: []
                    }; 

                    return new Promise( function (resolve_sub,reject_sub) {
                        const projectPath = path.join( tileSetsDirectory , name );
                        fs.mkdir( projectPath , (err) => {
                            if(err) reject_sub(err);
                            try {
                                fs.writeFile( path.join( projectPath , 'manifest.json') , JSON.stringify(ManifestObject,null,4), (writeErr) => {
                                    if(writeErr) reject_sub(err);
                                    resolve_sub(projectPath);
                                });
                            }
                            catch(exceptionErr) {
                                reject_sub(exceptionErr);
                            }
                        });
                    });
                })
                .then( projectPath => {
                    this.createDirectory( path.join( projectPath, 'textures' ) ).then( _ => {
                        resolve(true);
                    });
                })
                .catch( err => {
                    console.error(err);
                    reject(err);
                });
            });
        }

        writeTileSet(data) {
            return new Promise( (resolve,reject) => {
                const manifestPath = path.join( data.path , 'manifest.json' );
                delete data.path;
                delete data.texturesPath;
                fs.writeFile( manifestPath , JSON.stringify(data,null,4) , function(err) {
                    if(err) reject(err);
                    console.log('TileSet writed!');
                    resolve(true);
                } );
            });
        }

        objectIsTileManifest(tileObject) {
            const keyArr = ['width','height','textures','name'];
            for(let i = 0;i<keyArr.length;i++) {
                if( tileObject.hasOwnProperty(keyArr[i]) === false ) return false;
            }
            return true;
        }

        ready() {
            this.initialized = true;
            this.HTMLButton_addCube.removeAttribute('disabled');
            this.HTMLButton_deleteCube.removeAttribute('disabled');
            this.emit('ready',true);
        }

    }
    cubeViewManager = new Manager();

    /*
        BUTTON ADD CUBE
    */
    cubeViewManager.HTMLButton_addCube.addEventListener('click',(e) => {
        if(cubeViewManager.initialized === false) return;
        cubeViewManager.HTMLButton_addCube.blur();
        dialog.showOpenDialog({properties: ['openFile']},(fileNames) => {
            if(fileNames === undefined) return;

            const filePath          = fileNames[0];
            const destinationPath   = path.join( cubeViewManager.tileData.texturesPath , path.basename( filePath ) );

            if(filePath !== destinationPath) {
                const readStream    = fs.createReadStream( filePath );
                const writeStream   = fs.createWriteStream( destinationPath );

                readStream.on('open',_=> {
                    readStream.pipe(writeStream);
                });

                readStream.on('end', _=> {
                    cubeViewManager.tileData.textures.push(destinationPath);
                    cubeViewManager.writeTileSet(cubeViewManager.tileData).then( _=> {
                        cubeViewRenderer.spawnGeometry(destinationPath);
                    });
                });
            }
            else {
                cubeViewManager.tileData.textures.push(destinationPath);
                cubeViewManager.writeTileSet(cubeViewManager.tileData).then( _=> {
                    cubeViewRenderer.spawnGeometry(destinationPath);
                });
            }

        });
    });

    /*
        BUTTON DELETE CUBE
    */
    cubeViewManager.HTMLButton_deleteCube.addEventListener('click',(e) => {
        console.log('delete cube clicked');
        // Get selected cube!
    });

    /*
        BUTTON LOAD TILESET
    */
    cubeViewManager.HTMLButton_loadTileSet.addEventListener('click',(e) => {
        cubeViewManager.HTMLButton_loadTileSet.blur();
        dialog.showOpenDialog({properties: ['openDirectory']},(directoryArr) => {
            if(directoryArr === undefined) return;
            
            const manifest = path.join( directoryArr[0] , 'manifest.json' );
            fs.readFile(manifest,(err,buffer) => {
                if(err) throw new Error(err); 
                try {
                    var ManifestObject = JSON.parse(buffer.toString());
                }
                catch(exceptionErr) {
                    console.warn(exceptionErr);
                }

                if(cubeViewManager.objectIsTileManifest(ManifestObject) === true) {
                    if(cubeViewManager.initialized === true) {
                        cubeViewRenderer.delete();
                    }
                    cubeViewManager.init(ManifestObject.name);
                }
                else {
                    console.warn('Not a valid tileSet object!');
                }
            });
        });
    });

});