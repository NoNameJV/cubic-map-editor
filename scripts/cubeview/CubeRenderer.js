let cubeViewMainLoop;

document.addEventListener('DOMContentLoaded',(event) => {
    // Require packages 
    const events = require('events');
    const async  = require('async');


    const ISlotManagerConstructor = {
        lineCount: 4
    }
    
    class SlotManager {
        constructor(opts) {
            Object.assign(this,ISlotManagerConstructor,opts);
        }
    }

    // Interface
    const ICubeRndrDefault = {
        rotateSpeed: 0.002,
        cubesArr: [],
        mouseDown: false,
        mouseX: 0,
        mouseY: 0,
        renderingWidth: 380,
        renderingHeight: 250,
        cubeSize: 0.4
    }

    class cubeRenderer extends events {

        constructor(tileData) {
            super(); 

            // Global properties
            this.tileData = tileData;
            Object.assign(this,ICubeRndrDefault);

            // Replace by slotManager class!
            this.slots = [
                {x: -1.5,y: 1.5},
                {x: -0.5,y: 1.5},
                {x: 0.5,y: 1.5},
                {x: 1.5,y: 1.5},
                {x: -1.5,y: 0.65},
                {x: -0.5,y: 0.65},
                {x: 0.5,y: 0.65},
                {x: 1.5,y: 0.65},
                {x: -1.5,y: -0.15}
            ];

            // Create WebGL renderer
            this.webglRenderer      = new THREE.WebGLRenderer({ alpha: true });
            this.webglRenderer.setSize( this.renderingWidth , this.renderingHeight );
            cubeViewManager.HTMLRootSection.appendChild(this.webglRenderer.domElement);
            this.domElement         = this.webglRenderer.domElement;
            this.rayCaster          = new THREE.Raycaster();
            this.mouse              = new THREE.Vector2();
            this.intersectObjects   = [];
            this.selectedObject     = null;

            // Create default scene and add camera.
            this.scene   = new THREE.Scene();
            this.camera  = new THREE.PerspectiveCamera(70, this.renderingWidth / this.renderingHeight, 1, 10 );
            this.camera.position.set(0, 0, 3);
            this.scene.add(this.camera);

            let rollOverGeometry = new THREE.BoxGeometry(this.cubeSize, this.cubeSize, this.cubeSize);
            rollOverGeometry = new THREE.EdgesGeometry(rollOverGeometry);
            const rollOverMaterial = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 3 });
            this.rollOver = new THREE.LineSegments( rollOverGeometry, rollOverMaterial )
            this.rollOver.renderOrder = 0;
            this.rollOver.position.set(0, 0, 0);
            this.rollOver.visible = false;
            this.scene.add(this.rollOver);
            
            // Add default cubes to the scene!
            async.each(this.tileData.textures, (texture,callback) => {
                this.spawnGeometry(texture);
                callback();
            }, err => {
                if(err) throw new Error(err);
                console.log('Initialized done!');
                cubeViewManager.ready();
                window.requestAnimationFrame(cubeViewMainLoop);
                this.emit('initialized',true);
            });
            
        }   

        // Spawn geometry in the cubeView
        spawnGeometry(texture) {
            // TODO : Add type of the geometry
            const geometry  = new THREE.CubeGeometry( this.cubeSize , this.cubeSize , this.cubeSize );
            const material  = new THREE.MeshBasicMaterial({
                color: null,
                map: new THREE.TextureLoader().load(texture)
            });
            const mesh      = new THREE.Mesh( geometry, material );
            const position  = this.slots.shift();
            mesh.position.x = position.x; 
            mesh.position.y = position.y;
            mesh.rotation.set( this.rollOver.rotation.x , this.rollOver.rotation.y , this.rollOver.rotation.z );
            this.cubesArr.push(mesh);
            this.scene.add(mesh);
        }

        // Reset Renderer
        delete() {
            if(cubeViewManager.initialized === false) return;
            console.log('Cube view reset!');
            window.cancelAnimationFrame(this.main);
            cubeViewManager.HTMLRootSection.removeChild(this.domElement);
            this.emit('deleted');
            delete this;
        }

    }

    // When initRenderer is catched!
    cubeViewManager.on('initRenderer',tileData => {
        cubeViewRenderer = new cubeRenderer(tileData);

        // ISSUE: Why event handler does'nt work ?
        cubeViewRenderer.domElement.addEventListener('mousemove', onMouseMove, false);
        cubeViewRenderer.domElement.addEventListener('mousedown', onMouseDown, false);
        cubeViewRenderer.domElement.addEventListener('mouseup', onMouseUp, false);  
    });

    /*
        MAIN LOOP 
    */
    cubeViewMainLoop = function() {
        window.requestAnimationFrame(cubeViewMainLoop);
        cubeViewRenderer.camera.updateMatrixWorld(true);
        cubeViewRenderer.webglRenderer.render( cubeViewRenderer.scene, cubeViewRenderer.camera );
    }

    /*
        MOUSE EVENTS 
    */
    function onMouseMove(evt) {
        if (cubeViewRenderer.mouseDown === false) return;
        evt.preventDefault();
        //cubeViewRenderer.mouse.set((evt.clientX / cubeViewRenderer.renderingWidth) * 2 - 1, -(evt.clientY / cubeViewRenderer.renderingHeight) * 2 + 1);
        if(evt.button === 2) {
            const deltaX = (evt.clientX - cubeViewRenderer.mouseX) / 100;
            const deltaY = (evt.clientY - cubeViewRenderer.mouseY) / 100;
            cubeViewRenderer.mouseX = evt.clientX;
            cubeViewRenderer.mouseY = evt.clientY;
            async.each(cubeViewRenderer.cubesArr,(mesh,callback) => {
                mesh.rotation.x += deltaY;
                mesh.rotation.y += deltaX;
                cubeViewRenderer.rotation = mesh.rotation;
                callback();
            });
            cubeViewRenderer.rollOver.rotation.x += deltaY;
            cubeViewRenderer.rollOver.rotation.y += deltaX;
        }
    }

    function onMouseDown(evt) {
        evt.preventDefault();
        const domRECT = cubeViewRenderer.domElement.getBoundingClientRect();
        cubeViewRenderer.mouse.set(
            ( ( evt.clientX - domRECT.left ) / cubeViewRenderer.domElement.width ) * 2 - 1, 
            - ( ( evt.clientY - domRECT.top )  / cubeViewRenderer.domElement.height ) * 2 + 1
        );

        cubeViewRenderer.camera.updateMatrixWorld();
		cubeViewRenderer.rayCaster.setFromCamera(cubeViewRenderer.mouse, cubeViewRenderer.camera);
		const intersects = cubeViewRenderer.rayCaster.intersectObjects(cubeViewRenderer.scene.children);
        if(intersects.length > 0 && evt.button === 0) {
            let intersect = intersects[0];
            cubeViewRenderer.rollOver.position.setFromMatrixPosition( intersect.object.matrixWorld );
            cubeViewRenderer.rollOver.rotation = cubeViewRenderer.rotation;
            cubeViewRenderer.rollOver.visible = true;
        }
        cubeViewRenderer.mouseDown = true;
        cubeViewRenderer.mouseX = evt.clientX;
        cubeViewRenderer.mouseY = evt.clientY;
    }

    function onMouseUp(evt) {
        evt.preventDefault();
        cubeViewRenderer.mouseDown = false;
    }

});