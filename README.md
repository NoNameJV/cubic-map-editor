# Cubic map editor

## Installation 

```
npm install 
npm start
``` 

## Structure de map (prototype) 

```json
{
    "tileSet": "name",
    "width": 32,
    "height": 32,
    "layers": {
        "layerName": {
            "merged": true,
            "visible": true,
            "content": [
                {
                    "geometry": "cube",
                    "position": [0,0,0]
                },
                {
                    "geometry": "slope",
                    "rotation": 90,
                    "position": [1,0,0]
                }
            ]
        }
    }
}
```